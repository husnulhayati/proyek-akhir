<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Profile;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;


class AuthController extends Controller
{
    public function home()
    {
        return view('welcome');
    }

    // login view
    public function login()
    {
        return view('auth.login');
    }

    public function postlogin(Request $request)
    {
        $field = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $ingat = (bool)$request->rememberMe;

        if (Auth::attempt([
            $field => $request->email,
            'password' => $request->password,
        ], $ingat)) {
            return redirect('/');
        }
        return redirect('/login')->with('status', 'password anda salah');
    }

    //register view
    public function register()
    {
        return view('auth.register');
    }

    //register
    public function postregister(Request $request)
    {
        if ($request->agreeterm == true) {
            $request->validate([
                'username' => 'required',
                'nama' => 'required',
                'email' => 'required|email',
                'password' => 'required'
            ]);

            if ($request->password == $request->password2) {

                $user = new User;
                $user->role = 'user';
                $user->username = $request->username; // mengambil dari requst name="nama
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->remember_token = Str::random(60);
                $user->save();

                $request->request->add(['user_id' => $user->id, 'nama' => $request->nama]);
                Profile::create($request->all());

                return redirect('/login');
            } else {
                return redirect('/register')->with('status', 'Password yang anda masukan tidak sama');
            }
        } else {
            return redirect('/register')->with('status1', 'Anda harus mecentang persyaratan terlebih dahulu');
        }
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('auth.show', compact('user'));
    }


/**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        // dd(Crypt::decryptString($user->password));

        return view('auth.edit', compact('user'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {   $request->validate([
            'nama' => 'required',
            'bio' => 'required',
            'alamat' => 'required|email',
            'foto' => 'required'
        ]);
        if ($request->hasFile('foto')) {
            $request->file('foto')->move('images/', $request->file('foto')->getClientOriginalName());
            Profile::where('user_id', $id)
                ->update(['nama' => $request->nama, 'foto' => $request->file('foto')->getClientOriginalName()]);
            // table user
            User::where('id', $id)
                ->update(['username' => $request->username, 'email' => $request->email, 'password' => Hash::make($request->password)]);

        } else {
            Alert::eror('gagal', 'Profile gagal di update');
            return redirect('/profile/edit/{{$user->$id}}')->with('eror', 'data anda gagal di update');
        }
        Alert::success('Berhasil', 'Profile berhasil di update');
        return redirect('/profile/show/{{user->$id}}')->with('sukses', 'data anda berhasil di update');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
