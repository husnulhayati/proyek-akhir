@extends('layout.app', ['class' => 'bg-default'])

@section('content')
<div class="main-content">
    <nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark">
<div class="container px-4">
<!--a class="navbar-brand" href="https://argon-dashboard-laravel.creative-tim.com/home">
    <img src="https://argon-dashboard-laravel.creative-tim.com/argon/img/brand/white.png">
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-main" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button-->
<div class="collapse navbar-collapse" id="navbar-collapse-main">
    <!-- Collapse header -->
    <!--div class="navbar-collapse-header d-md-none">
        <div class="row">
            <div class="col-6 collapse-brand">
                <a href="https://argon-dashboard-laravel.creative-tim.com/home">
                    <img src="https://argon-dashboard-laravel.creative-tim.com/argon/img/brand/blue.png">
                </a>
            </div>
            <div class="col-6 collapse-close">
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                    <span></span>
                    <span></span>
                </button>
            </div>
        </div>
    </div-->
    <!-- Navbar items -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link nav-link-icon" href="/">
                <i class="ni ni-planet"></i>
                <span class="nav-link-inner--text">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link nav-link-icon" href="/register">
                <i class="ni ni-circle-08"></i>
                <span class="nav-link-inner--text">Register</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link nav-link-icon" href="/login">
                <i class="ni ni-key-25"></i>
                <span class="nav-link-inner--text">Login</span>
            </a>
        </li>
    </ul>
</div>
</div>
</nav>               
<div class="header bg-gradient-primary py-7 py-lg-8">
<div class="container">
<div class="header-body text-center mb-7">
    <div class="row justify-content-center">
        <div class="col-lg-5 col-md-6">
            <h1 class="text-white">Selamat Datang di BahasBareng</h1>
        </div>
    </div>
</div>
</div>
<div class="separator separator-bottom separator-skew zindex-100">
<svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
</svg>
</div>
</div>    <div class="container mt--8 pb-5">
<div class="row justify-content-center">
    <div class="col-lg-5 col-md-7">
        <div class="card bg-secondary shadow border-0">
            <div class="card-body px-lg-5 py-lg-5">
                <div class="text-center text-muted mb-4">
                    <h2>
                        Silakan buat akun baru
                    </h2>
                </div>
                <div class="signin-form">    
                    <form method="POST" action="/postregister" class="register-form" id="register-form">
                        @csrf
                        <div class="form-group{{ $errors->has('username') ? ' has-danger' : '' }} mb-3">
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                                </div>
                                <input class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" placeholder="{{ __('Username') }}" type="username" name="username" value="{{ old('username') }}" required autofocus>
                            </div>
                            @if ($errors->has('username'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('nama') ? ' has-danger' : '' }} mb-3">
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                                </div>
                                <input class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" placeholder="{{ __('Nama') }}" type="name" name="nama" value="{{ old('nama') }}" required autofocus>
                            </div>
                            @if ($errors->has('nama'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('nama') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }} mb-3">
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                </div>
                                <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email') }}" type="email" name="email" value="{{ old('email') }}" value="admin@argon.com" required autofocus>
                            </div>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                </div>
                                <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ __('Password') }}" type="password" value="secret" required>
                            </div>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        @if (session('status'))
                        <div class="alert alert-danger" style="margin-top: -8%">
                            {{ session('status') }}
                        </div>
                        @endif

                        <div class="form-group{{ $errors->has('password2') ? ' has-danger' : '' }}">
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                </div>
                                <input class="form-control{{ $errors->has('password2') ? ' is-invalid' : '' }}" name="password2" placeholder="{{ __('Ulangi Password') }}" type="password" value="secret" required>
                            </div>
                            @if ($errors->has('password2'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                    <strong>{{ $errors->first('password2') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="custom-control custom-control-alternative custom-checkbox">
                            <input class="custom-control-input" name="agreeterm" id="agree-term" type="checkbox"}}>
                            <label class="custom-control-label" for="agree-term">
                                <span class="text-muted">{{ __('I agree with all
                                    statements in')}}</span> <a href="#" class="term-service">{{ __('Terms of service')}}</a>
                            </label>
                        </div>

                        @if (session('status1'))
                            <div class="alert alert-danger text-danger" style="margin-top: -8%">
                                {{ session('status1') }}
                            </div>
                        @endif
                        
                        <div class="custom-control custom-control-alternative custom-checkbox">
                            <input class="custom-control-input" name="remember" id="customCheckLogin" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="customCheckLogin">
                                <span class="text-muted">{{ __('Remember me') }}</span>
                            </label>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary my-4">{{ __('Sign Up') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">  
            <div class="signin-content">
                    <p> I am already a member <a href="/login" class="form submit">Log In</a> </p>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection


